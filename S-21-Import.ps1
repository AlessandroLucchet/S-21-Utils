﻿<#
	Converte un file CSV contenenti i campi del PDF S-21 in un file CSV formattato per mese.
	Puoi esportare il campi del file CSV con PDF-XChange.
#>
Add-Type -AssemblyName System.Windows.Forms

# Chiedi file input
$FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property @{ 
    InitialDirectory = [Environment]::GetFolderPath('Desktop') 
    Filter = 'File CSV (*.csv)|*.csv'
}
$null = $FileBrowser.ShowDialog()

# Input / output
$CSVinPath = $FileBrowser.FileName
$CSVoutPath = "$PSScriptRoot\S21out.csv"
$CSVin = Import-Csv $CSVinPath -Delimiter ";"
$CSVout = @()
$MonthsNames = @('January','February','March','April','May','June','July','August','September','October','November','December')

# Scorri file
foreach($riga in $CSVin)
{
    foreach($modulo in $(1,2))
    {
        foreach($mese in $(1..12))
        {
            $meseModulo = &{if($mese -lt 9){$mese + 4}else{$mese - 8}}
            $CSVout += New-Object PSObject -Property @{
                Nome = $riga.Name
                Mese = "$([int]($riga.(&{if($modulo -eq 1){"Service Year"}else{"Service Year_2"}}))-(&{if($mese -ge 9){1}else{0}}))-$mese-1"
                Pubblicazioni = $riga."$($modulo)-Place_$($meseModulo)"
                Video = $riga."$($modulo)-Video_$($meseModulo)"
                Ore = $riga."$($modulo)-Hours_$($meseModulo)"
                Visite = $riga."$($modulo)-RV_$($meseModulo)"
                Studi = $riga."$($modulo)-Studies_$($meseModulo)"
                Note = $riga.("Remarks$($MonthsNames[$mese-1])"+(&{if($modulo -eq 2){"_2"}}))
            }
        }
    }
}

# Esporta
$CSVout | Select-Object Nome,Mese,Ore,Visite,Video,Pubblicazioni,Studi,Note | Export-Csv $CSVoutPath -NoTypeInformation -Delimiter ";"