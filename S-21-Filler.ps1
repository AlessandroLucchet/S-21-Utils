<#
	Compila un file PDF S-21 a partire da un file CSV
#>

function Save-PdfField {
	[CmdletBinding()]
	param
	(
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[Hashtable]$Fields,
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[ValidatePattern('\.pdf$')]
		[ValidateScript({ Test-Path -Path $_ -PathType Leaf })]
		[string]$InputPdfFilePath,
		
		[Parameter(Mandatory)]
		[ValidateNotNullOrEmpty()]
		[ValidatePattern('\.pdf$')]
		[ValidateScript({ -not (Test-Path -Path $_ -PathType Leaf) })]
		[string]$OutputPdfFilePath
		
	)
	begin
	{
		$ErrorActionPreference = 'Stop'
	}
	process
	{
		try
		{
			$reader = New-Object iTextSharp.text.pdf.PdfReader -ArgumentList $InputPdfFilePath
			$stamper = New-Object iTextSharp.text.pdf.PdfStamper($reader, [System.IO.File]::Create($OutputPdfFilePath))
			
			## Apply all hash table elements into the PDF form
			foreach ($j in $Fields.GetEnumerator())
			{
				$null = $stamper.AcroFields.SetField($j.Key, $j.Value)
			}
		}
		catch
		{
			$PSCmdlet.ThrowTerminatingError($_)
		}
		finally
		{
			## Close up shop 
			$stamper.Close()
			Get-Item -Path $OutputPdfFilePath
		}
	}
}

# Trova i file di input
$DataPath = (Get-ChildItem "$PSScriptRoot\*.csv" | Select-Object -First 1 -Property FullName).FullName
$S21Path = (Get-ChildItem "$PSScriptRoot\*.pdf" | Select-Object -First 1 -Property FullName).FullName
$DllPath = (Get-ChildItem "$PSScriptRoot\itextsharp.dll" | Select-Object -First 1 -Property FullName).FullName
if(!$DataPath -or !$S21Path -or !$DllPath){
	Write-Host "Attenzione! Nel percorso dello script deve esserci:`n- Il file S-21.pdf vuoto`n- Un file CSV con N record e con i nomi delle colonne identici al nome dei campi del PDF`n- La libreria iTextSharp 5 scaricabile da: https://github.com/itext/itextsharp/releases"
	pause
	exit
}

# Carica la libreria iTextSharp
[System.Reflection.Assembly]::LoadFrom($DllPath) | Out-Null

# Importa i dati dal CSV
$Data = Import-Csv $DataPath -Delimiter "`t"

# Crea la cartella di output
$OutputDir = "$PSScriptRoot\Output"
if(-not (Test-Path -Path $OutputDir)){ New-Item -Path $OutputDir -ItemType Directory | Out-Null }

# Scorri dati e creai i PDF
foreach($Publisher in $Data) {
    $Fields = @{}
    $Publisher.psobject.properties | Foreach { $Fields[$_.Name] = $_.Value }
    Save-PdfField -InputPdfFilePath $S21Path -OutputPdfFilePath ("$PSScriptRoot\Output\S-21 {0} ({1}).pdf" -f $Publisher."900_1_Text",$Publisher."900_11_Year_C") -Fields $Fields
}